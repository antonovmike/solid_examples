# Single Responsibility Principle (SRP)


class User:
    def __init__(self, name, email):
        self.name = name
        self.email = email


class UserRepository:
    def save(self, user):
        print(f"Saved {user.name}")


class EmailService:
    def send_email(self, user):
        print(f"Email sent to {user.name}")


alice = User("Alice", "alice@mail.com")
print(alice.name)

user_repository = UserRepository()
user_repository.save(alice)

email_service = EmailService()
email_service.send_email(alice)
