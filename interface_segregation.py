# Interface Segregation Principle (ISP)
from abc import ABC, abstractmethod


# UserRepository and EmailService are interfaces and defines the contract to be implemented by the class
class UserRepository(ABC):
    @abstractmethod
    def save(self, user):
        pass


class EmailService(ABC):
    @abstractmethod
    def send_email(self, user):
        pass


class Worker:
    def __init__(self, user_repository: UserRepository, email_service: EmailService):
        self.user_repository = user_repository
        self.email_service = email_service

    def work(self):
        pass

    def eat(self):
        pass

    def save(self, user):
        self.user_repository.save(user)

    def send_email(self, user):
        self.email_service.send_email(user)


# Creating specific implementations for UserRepository and EmailService
class ConcreteUserRepository(UserRepository):
    def save(self, user):
        print(f"Save user: {user}")


class ConcreteEmailService(EmailService):
    def send_email(self, user):
        print(f"Send email to: {user}")


user_repository = ConcreteUserRepository()
email_service = ConcreteEmailService()

worker = Worker(user_repository, email_service)

worker.save("Alice")
worker.send_email("Alice")
