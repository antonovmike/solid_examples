# Dependency Inversion Principle (DIP)


from abc import ABC, abstractmethod


class Switchable(ABC):
    @abstractmethod
    def turn_on(self):
        pass

    @abstractmethod
    def turn_off(self):
        pass

    @abstractmethod
    def is_on(self):
        pass


class LightBulb(Switchable):
    def __init__(self):
        self.is_on_state = False

    def turn_on(self):
        self.is_on_state = True
        print("Light is on")

    def turn_off(self):
        self.is_on_state = False
        print("Light is off")

    def is_on(self):
        return self.is_on_state


class ElectricPowerSwitch:
    def __init__(self, switchable: Switchable):
        self.switchable = switchable

    def press(self):
        if self.switchable.is_on():
            self.switchable.turn_off()
        else:
            self.switchable.turn_on()


light_bulb = LightBulb()
switch = ElectricPowerSwitch(light_bulb)

switch.press()
switch.press()
