# Open/Closed Principle (OCP)

from abc import ABC, abstractmethod


class Payment(ABC):
    @abstractmethod
    def process_payment(self, amount):
        pass


class CreditCardPayment(Payment):
    def process_payment(self, amount):
        print(f"Processing credit card payment of {amount}")


class OnlinePayment(Payment):
    def process_payment(self, amount):
        print(f"Processing online payment of {amount}")


class CryptoCurrencyPayment(Payment):
    def process_payment(self, amount):
        print(f"Processing crypto payment of {amount}")


# New class that uses a specific payment method passed as an argument
# Buiseness logic should be implemented here
class PaymentProcessing:
    def concrete_payments(self, payment_method: Payment, amount):
        payment_method.process_payment(amount)


credit_card_payment = CreditCardPayment()
some_object = PaymentProcessing()
some_object.concrete_payments(credit_card_payment, 100)

online_payment = OnlinePayment()
some_object.concrete_payments(online_payment, 200)

crypto_payment = CryptoCurrencyPayment()
some_object.concrete_payments(crypto_payment, 300)
