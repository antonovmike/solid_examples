# Solid Examples

In the process of studying SOLID, I wrote a micro-article about these principles and tried to give the utmost useful examples in Python. 
How useful these examples are is for you to judge.

![How can applying the SOLID principles make the code better?](https://dev.to/antonov_mike/how-can-applying-the-solid-principles-make-the-code-better-3fam)
